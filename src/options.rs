#[derive(StructOpt, Debug)]
pub struct SubCommandReadCoils {
    #[structopt(name = "address", help = "Start address to read from")]
    pub address: u16,
    #[structopt(name = "quantity", help = "Num bits")]
    pub quantity: u16,
}

#[derive(StructOpt, Debug)]
pub struct SubCommandReadDiscreteInputs {
    #[structopt(name = "address", help = "Start address to read from")]
    pub address: u16,
    #[structopt(name = "quantity", help = "Num bits")]
    pub quantity: u16,
}

#[derive(StructOpt, Debug)]
pub struct SubCommandWriteSingleCoil {
    #[structopt(name = "address", help = "Start address to write coil")]
    pub address: u16,
    #[structopt(name = "coil", help = "Coil")]
    pub coil: bool,
}

#[derive(StructOpt, Debug)]
pub struct SubCommandWriteMultipleCoils {
    #[structopt(name = "address", help = "Start address to write coil")]
    pub address: u16,
    #[structopt(name = "coil", help = "Coil")]
    pub coils: Vec<bool>,
}

#[derive(StructOpt, Debug)]
pub struct SubCommandReadInputRegisters {
    #[structopt(name = "address", help = "Start address to read from")]
    pub address: u16,
    #[structopt(name = "quantity", help = "Num bits")]
    pub quantity: u16,
}

#[derive(StructOpt, Debug)]
pub struct SubCommandReadHoldingRegisters {
    #[structopt(name = "address", help = "Start address to read from")]
    pub address: u16,
    #[structopt(name = "quantity", help = "Num bits")]
    pub quantity: u16,
}

#[derive(StructOpt, Debug)]
pub struct SubCommandWriteSingleRegister {
    #[structopt(name = "address", help = "Start address to read from")]
    pub address: u16,
    #[structopt(name = "data", help = "Data")]
    pub data: u16,
}

#[derive(StructOpt, Debug)]
pub struct SubCommandWriteMultipleRegisters {
    #[structopt(name = "address", help = "Start address to read from")]
    pub address: u16,
    #[structopt(name = "data", help = "Data")]
    pub data: Vec<u16>,
}

#[derive(StructOpt, Debug)]
pub struct SubCommandReadWriteMultipleRegisters {
    #[structopt(name = "read_address", help = "Read start address to read from")]
    pub read_address: u16,
    #[structopt(name = "read_quantity", help = "Write n bits")]
    pub read_quantity: u16,
    #[structopt(name = "write_address", help = "Write start address to write from")]
    pub write_address: u16,
    #[structopt(name = "write_data", help = "Write data")]
    pub write_data: Vec<u16>,
}

arg_enum! {
    #[derive(Debug)]
    pub enum Library {
        TokioModbus,
        Libmodbus,
    }
}


#[derive(StructOpt, Debug)]
pub enum Fc {
    #[structopt(
        name = "read_coils",
        raw(aliases = r#"&["0x01", "1", "read_coils", "ReadCoils"]"#)
    )]
    ReadCoils(SubCommandReadCoils),
    #[structopt(
        name = "read_discrete_inputs",
        raw(aliases = r#"&["0x02", "2", "read_discrete_inputs", "ReadDiscreteInputs"]"#)
    )]
    ReadDiscreteInputs(SubCommandReadDiscreteInputs),
    #[structopt(
        name = "write_single_coil",
        raw(aliases = r#"&["0x05", "5", "write_single_coil", "WriteSingleCoil"]"#)
    )]
    WriteSingleCoil(SubCommandWriteSingleCoil),
    #[structopt(
        name = "write_multiple_coils",
        raw(aliases = r#"&["0x0F", "F", "write_multiple_coils", "WriteMultipleCoils"]"#)
    )]
    WriteMultipleCoils(SubCommandWriteMultipleCoils),
    #[structopt(
        name = "read_input_registers",
        raw(aliases = r#"&["0x04", "4", "read_input_registers", "ReadInputRegisters"]"#)
    )]
    ReadInputRegisters(SubCommandReadInputRegisters),
    #[structopt(
        name = "read_holding_registers",
        raw(aliases = r#"&["0x03", "3", "read_holding_registers", "ReadHoldingRegisters"]"#)
    )]
    ReadHoldingRegisters(SubCommandReadHoldingRegisters),
    #[structopt(
        name = "write_single_register",
        raw(aliases = r#"&["0x06", "6", "write_single_register", "WriteSingleRegister"]"#)
    )]
    WriteSingleRegister(SubCommandWriteSingleRegister),
    #[structopt(
        name = "write_multiple_registers",
        raw(aliases = r#"&["0x10", "10", "write_multiple_registers", "WriteMultipleRegisters"]"#)
    )]
    WriteMultipleRegisters(SubCommandWriteMultipleRegisters),
    #[structopt(
        name = "read_write_multiple_registers",
        raw(
            aliases = r#"&["0x17", "17", "read_write_multiple_registers", "ReadWriteMultipleRegisters"]"#
        )
    )]
    ReadWriteMultipleRegisters(SubCommandReadWriteMultipleRegisters),
    #[structopt(name = "custom")]
    Custom,
}

#[derive(StructOpt, Debug)]
pub struct SubCommandRtu {
    #[structopt(subcommand, name = "fc", help = "Modbus function code")]
    pub fc: Fc,
    #[structopt(raw(
            possible_values = "&Library::variants()",
            case_insensitive = "true",
        ),
        name = "library",
        short = "l",
        long = "library",
        default_value = "Libmodbus",
    )]
    pub library: Library,
    #[structopt(
        name = "tty_path",
        short = "t",
        long = "tty_path",
        default_value = "/dev/ttyUSB0",
        help = "Serial port path"
    )]
    pub tty_path: String,
    #[structopt(
        short = "b",
        long = "baud",
        default_value = "9600",
        help = "Baud rate"
    )]
    pub baud_rate: u32,
    #[structopt(
        long = "server_address",
        short = "a",
        help = "Modbus server address"
    )]
    pub server_address: u8,
    #[structopt(
        name = "request_to_send",
        long = "rts",
        help = "Sets the state of the RTS (Request To Send) control signal. The parameter `--rts` set the RTS control signal, omit this parameter to clear the signal."
    )]
    pub request_to_send: bool,
}

#[derive(StructOpt, Debug)]
pub struct SubCommandTcp {
    #[structopt(
        name = "address",
        short = "a",
        long = "addr",
        help = "Address and port of modbus server like 10.0.0.1:502"
    )]
    pub addr: String,
    #[structopt(subcommand, name = "fc", help = "Modbus function code")]
    pub fc: Fc,
}

#[derive(StructOpt, Debug)]
pub enum Backend {
    #[structopt(name = "rtu")]
    Rtu(SubCommandRtu),
    #[structopt(name = "tcp")]
    Tcp(SubCommandTcp),
}

#[derive(StructOpt, Debug)]
pub struct Opt {
    #[structopt(subcommand)]
    pub backend: Backend,
}
