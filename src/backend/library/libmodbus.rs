use options;
use libmodbus_rs::{Modbus, ModbusClient, ModbusRTU};



fn print_register(result: Vec<u16>) {
    println!("\n\t\t\tHigh Byte\tLow Byte\t16bit Word High and Low Byte");
    for (num, &reg) in result.iter().enumerate() {
        println!("{}\t\t\t{:08b} ({:3})\t{:08b} ({:3})\t{:016b} ({:5})", num, reg >> 8, reg >> 8, reg & 0xFF, reg & 0xFF, reg, reg);
    }
}

pub fn run(options: options::SubCommandRtu) {
        let tty_path = options.tty_path;

        let mut modbus = Modbus::new_rtu(&tty_path, 9600, 'N', 8, 1).expect("Could not create modbus context");
        modbus.set_slave(options.server_address).expect("Could not set server address");
        modbus.set_debug(true).expect("Could not set debug mode");
        modbus.connect().expect("Could not connect");


        match options.fc {
            // 01 (0x01) Read Coils
            options::Fc::ReadCoils(options) => {
                let quantity = options.quantity;
                let address = options.address;
                debug!("read_coils num: {} from addr:{}", quantity, address);
                let mut result = vec![0u8; quantity as usize];
                modbus.read_bits(address, quantity, &mut result).expect("Could not read bits");
                println!("{:?}", result);
            },
            // 02 (0x02) Read Discrete Inputs
            options::Fc::ReadDiscreteInputs(options) => {
                let quantity = options.quantity;
                let address = options.address;
                debug!("read discrete inputs");
                let mut result = vec![0u8; quantity as usize];
                modbus.read_input_bits(address, quantity, &mut result).expect("Could not read bits");
                println!("{:?}", result);

            },
            // 03 (0x03) Read Holding Registers
            options::Fc::ReadHoldingRegisters(options) => {
                let quantity = options.quantity;
                let address = options.address;
                debug!("read_holding_registers num: {} from addr: {}", quantity, address);
                let mut result = vec![0u16; quantity as usize];
                modbus.read_registers(address, quantity, &mut result).expect("could not read registers");

                print_register(result);
            },
            // 04 (0x04) Read Input Registers
            options::Fc::ReadInputRegisters(options) => {
                let quantity = options.quantity;
                let address = options.address;
                debug!("read input register num: {} from addr: {}", quantity, address);
                let mut result = vec![0u16; quantity as usize];
                modbus.read_input_registers(address, quantity, &mut result).expect("could not read input registers");

                print_register(result);
            },
            // 05 (0x05) Write Single Coil
            options::Fc::WriteSingleCoil(_options) => {
                unimplemented!("write single coil");
            },
            // 06 (0x06) Write Single Registers
            options::Fc::WriteSingleRegister(_options) => {
                unimplemented!("write single register");
            },
            options::Fc::ReadWriteMultipleRegisters(_options) => {
                unimplemented!("read/ write multiple registers");
            },
            options::Fc::WriteMultipleCoils(_options) => {
                unimplemented!("write multiple coils");
            },
            // 16 (0x10) Write Multiple Registers
            options::Fc::WriteMultipleRegisters(_options) => {
                unimplemented!("write multiple registers");
            },
            _ => {}
        };
}
