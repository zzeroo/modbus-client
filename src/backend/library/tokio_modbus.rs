use options;
use tokio_serial::{SerialPort, Serial};
use tokio_modbus::*;


pub fn run(options: options::SubCommandRtu) {
        let tty_path = options.tty_path;

        let mut settings = tokio_serial::SerialPortSettings::default();
        settings.baud_rate = options.baud_rate.into();

        let mut serial = Serial::from_path(&tty_path, &settings).unwrap();

        serial.set_exclusive(false).unwrap();
        // Setup RTS
        if options.request_to_send {
            serial.write_request_to_send(true).unwrap();
        } else {
            serial.write_request_to_send(false).unwrap();
        }


        // TokioModbus Lib
        let mut client = SyncClient::connect_rtu(&tty_path, &settings, options.server_address).unwrap();

        match options.fc {
            options::Fc::ReadCoils(options) => {
                debug!("read_coils num: {} from addr:{}", &options.quantity, &options.address);
                let result = client.read_coils(options.address, options.quantity).unwrap();
                println!("{:?}", result);
            },
            options::Fc::ReadHoldingRegisters(options) => {
                debug!("read_holding_registers num: {} from addr: {}", &options.quantity, &options.address);
                let result = client.read_holding_registers(options.address, options.quantity).unwrap();
                println!("{:?}", result);
            },
            options::Fc::ReadInputRegisters(options) => {
                debug!("read input register num: {} from addr: {}", &options.quantity, &options.address);
                let result = client.read_input_registers(options.address, options.quantity).unwrap();
                println!("{:?}", result);
            },
            options::Fc::ReadWriteMultipleRegisters(options) => {
                debug!("read/ write multiple registers");
                client.read_write_multiple_registers(options.read_address, options.read_quantity, options.write_address, &options.write_data).unwrap();
            },
            options::Fc::WriteMultipleCoils(options) => {
                debug!("write multiple coils");
                client.write_multiple_coils(options.address, &options.coils).unwrap();
            },
            options::Fc::WriteMultipleRegisters(options) => {
                debug!("write multiple registers");
                client.write_multiple_registers(options.address, &options.data).unwrap();
            },
            options::Fc::ReadDiscreteInputs(options) => {
                debug!("read discrete inputs");
                let result = client.read_discrete_inputs(options.address, options.quantity).unwrap();
                println!("{:?}", result);
            },
            options::Fc::WriteSingleCoil(options) => {
                debug!("write single coil");
                client.write_single_coil(options.address, options.coil).unwrap();
            },
            options::Fc::WriteSingleRegister(options) => {
                debug!("write single register");
                client.write_single_register(options.address, options.data).unwrap();
            },
            _ => {}
        };
}
