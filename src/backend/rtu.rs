use options;
use backend::library;


/// Modbus context with RTU backend
pub fn run(options: options::SubCommandRtu) {
    match options.library {
        options::Library::Libmodbus => { library::libmodbus::run(options); },
        options::Library::TokioModbus => { library::tokio_modbus::run(options); },
    }
}
