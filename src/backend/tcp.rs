use options;
use tokio_modbus::*;

/// Modbus context with TCP backend
pub fn run(options: options::SubCommandTcp) {
    let addr = options.addr.parse().unwrap();

    let mut client = SyncClient::connect_tcp(&addr).unwrap();
    match options.fc {
        options::Fc::ReadCoils(options) => {
            client.read_coils(options.address, options.quantity).unwrap();
        }
        options::Fc::WriteSingleCoil(options) => {
            client.write_single_coil(options.address, options.coil).unwrap();
        }
        _ => {}
    };
}
