#[macro_use]
extern crate log;
#[macro_use]
extern crate structopt;
#[macro_use]
extern crate clap;
extern crate futures;
extern crate libmodbus_rs;
extern crate tokio_core;
extern crate tokio_modbus;
extern crate tokio_serial;

use options::Opt;
use structopt::StructOpt;

mod backend;
mod options;

fn main() {
    let opt = Opt::from_args();
    match opt.backend {
        options::Backend::Rtu(opt) => backend::rtu::run(opt),
        options::Backend::Tcp(opt) => backend::tcp::run(opt),
    }
}
