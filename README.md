# modbus-client

Modbus Client based on the beautiful [tokio-modbus] crate.

[Homepage]| [API development]

|||
|:---|:------|
|**master:**|[![Build Status](https://gitlab.com/zzeroo/modbus-client/badges/master/build.svg)](https://gitlab.com/zzeroo/modbus-client/pipelines)|
|**development:**|[![Build Status](https://gitlab.com/zzeroo/modbus-client/badges/development/build.svg)](https://gitlab.com/zzeroo/modbus-client/pipelines)|



[Homepage]: https://zzeroo.gitlab.io/modbus-client/
[API development]: https://zzeroo.gitlab.io/modbus-client/modbus_client/index.html
[tokio-modbus]: https://github.com/slowtec/tokio-modbus
[CI]: https://gitlab.com/zzeroo/modbus-client/pipelines
[badge-master]: https://gitlab.com/zzeroo/modbus-client/badges/master/build.svg
